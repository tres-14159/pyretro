#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

setup(
	name = 'pyRetro',
	
	version = '2017.01.05.20.20',
	
	description = 'pyRetro is a MAME frontend written in pygame. It is focused for use in arcade machine in a cabinet in a public place.',
	
	long_description =
		'pyRetro is a MAME frontend written in pygame. ' + \
		'It is easy to use and nearly automatic configuration. ' + \
		'It is focused for use in arcade machine in a cabinet in a public place.'
)
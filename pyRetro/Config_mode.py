# -*- coding: utf-8 -*-
"""
   Copyright (C) 2011 Miguel de Dios

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   higher any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
"""

from Util import *
from Frontend import *
import math, os
from pprint import *

import pygame
import pygame.gfxdraw
from pygame.locals import *

class Config_mode:
	font_big_size = 40
	font_medium_size = 20
	font_normal_size = 12
	
	background_color = (204, 204, 204)
	font_color = (0, 0, 0)
	color_paint = (0, 0, 0)
	
	title_text = "CONFIG MODE"
	subtitle_text = "Change keys"
	
	def __init__(self, util_param = None, frontend_param = None):
		self.util = util_param
		self.frontend = frontend_param
		self.font_big = pygame.font.SysFont('Arial Bold', self.font_big_size)
		self.font_medium = pygame.font.SysFont('Arial', self.font_medium_size)
		self.font_normal = pygame.font.SysFont('Arial', self.font_normal_size)
	
	def blit_center_horizontal(self, image, y):
		screen_width = self.util.options["screen_width"]
		image_width = image.get_width()
		
		x = (screen_width - image_width) / 2
		self.frontend.screen.blit(image, (x, y))
	
	def paint_title(self):
		tempImage = self.font_big.render(self.title_text, False, self.font_color)
		self.blit_center_horizontal(tempImage, 2)
		
		pygame.draw.line(self.frontend.screen,
			self.color_paint,
			(0, self.font_big_size),
			(self.util.options["screen_width"], self.font_big_size),
			4)
		
		tempImage = self.font_medium.render(self.subtitle_text, False, self.font_color)
		self.frontend.screen.blit(tempImage, (0, self.font_big_size))
		
		pygame.draw.line(self.frontend.screen,
			self.color_paint,
			(0, self.font_big_size + self.font_medium_size + 5),
			(self.util.options["screen_width"], self.font_big_size + self.font_medium_size + 5),
			4)
	
	
	def paint_screen(self):
		self.frontend.screen.fill(self.background_color)
		self.paint_title()
		
		pos = self.font_big_size + self.font_medium_size + 15
		
		keys = {
			"UP" : K_UP,
			"DOWN" : K_DOWN,
			"RIGHT" : K_RIGHT,
			"LEFT" : K_LEFT,
			"SELECT GAME" : K_RETURN,
			"ADD FAVORITIES" : K_LALT,
			"SHOW FAVORITIES" : K_SPACE,
			"EXIT" : K_ESCAPE,
			"EXIT WITHOUT SHUTDOWN" : K_q}
		count = 0
		for (action, key) in keys.items():
			key_name = pygame.key.name(key)
			
			tempImage = self.font_medium.render(action, False, self.font_color)
			
			self.frontend.screen.blit(tempImage, (0, pos + (count * (self.font_medium_size + 10))))
			
			tempImage = self.font_medium.render(key_name, False, self.font_color)
			self.frontend.screen.blit(tempImage,
				(self.util.options["screen_width"] - tempImage.get_width(), pos + (count * (self.font_medium_size + 10))))
			
			count += 1
		
		pygame.gfxdraw.rectangle(self.frontend.screen, (0, 0, 200, 200), (0, 0, 0))
	
	def run(self):
		exit_var = False
		
		while not exit_var:
			self.paint_screen()
			
			self.frontend.waitFrame()
			pygame.display.update()
			
			pygame.event.pump()
			ev_list = pygame.event.get([pygame.KEYDOWN, pygame.QUIT])
			for ev in ev_list:
				done = True
				if ev.type == QUIT:
					exit_var = True
				elif ev.type == pygame.KEYDOWN:
					print(pygame.key.name(ev.key))
					exit_var = True
	
#!/bin/bash
#
#   Copyright (C) 2013 Miguel de Dios
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 3 of the License, or
#   higher any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software Foundation,
#   Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA


# Generate the version from
# SVN revision
# and date

svn_revision=`svn info | grep Revision: | tr -d 'Revision: '`
date=`date +"%Y.%m.%d"`

pyretro_version="$date.r$svn_revision"

for param in $@
do
	if [ $param = "-h" -o $param = "--help" ]
	then
		echo "There are not options. Only make a DEB package for PyRetro"
		
		exit 0
	fi
done

echo "Test if you have all the needed tools to make the packages."
whereis dpkg-deb | cut -d":" -f2 | grep dpkg-deb > /dev/null
if [ $? = 1 ]
then
	echo "No found \"dpkg-deb\" aplication, please install."
	exit 1
else
	echo "Found \"dpkg-debs\"."
fi

whereis dpkg-buildpackage | cut -d":" -f2 | grep dpkg-buildpackage > /dev/null
if [ $? = 1 ]
then
	echo " \"dpkg-buildpackage\" aplication not found, please install."
	exit 1
else
	echo "Found \"dpkg-buildpackage\"."
fi

cd ..

echo "Make temporary directory for to job in \"temp_package\"."
mkdir -p temp_package/usr/share/pyRetro/tools
mkdir -p temp_package/usr/share/doc/pyRetro
mkdir -p temp_package/usr/share/applications
mkdir -p temp_package/usr/games/

echo "Make directory system tree for package."
echo " - Copy the docs"
cp ChangeLog temp_package/usr/share/doc/pyRetro
cp README temp_package/usr/share/doc/pyRetro
cp TODO temp_package/usr/share/doc/pyRetro
echo " - Copy the code"
cp -R pyRetro/* temp_package/usr/share/pyRetro
cp -R tools/* temp_package/usr/share/pyRetro/tools
echo " - Copy menu entry"
cp pyRetro.desktop temp_package/usr/share/applications/
echo " - Copy DEB files"
cp -R DEBIAN temp_package
echo " - Create shell to call pyRetro"
echo "#!/bin/bash" >> temp_package/usr/games/pyRetro
echo "cd /usr/share/pyRetro" >> temp_package/usr/games/pyRetro
echo "python pyRetro.py \$@" >> temp_package/usr/games/pyRetro
chmod 755 -R temp_package/usr/games/pyRetro

chmod 755 -R temp_package/DEBIAN


echo "Remove the SVN files and other temp files."
for item in `find temp_package`
do
	echo -n "."
	
	echo $item | grep "svn" > /dev/null
	#last command success
	if [ $? -eq 0 ]
	then
		rm -rf $item
	fi
	
	echo $item | grep ".pyc" > /dev/null
	#last command success
	if [ $? -eq 0 ]
	then
		rm -rf $item
	fi
	
	echo $item | grep "make_deb_package.sh" > /dev/null
	#last command success
	if [ $? -eq 0 ]
	then
		rm -rf $item
	fi
done
echo "END"

echo "Calculate md5sum for md5sums package control file."
for item in `find temp_package`
do
	echo -n "."
	if [ ! -d $item ]
	then
		echo $item | grep "DEBIAN" > /dev/null
		#last command success
		if [ $? -eq 1 ]
		then
			md5=`md5sum $item | cut -d" " -f1`
			
			#delete "temp_package" in the path
			final_path=${item#temp_package}
			echo  $md5" "$final_path >> temp_package/DEBIAN/md5sums
		fi
	fi
done
echo "END"

echo "Make the package \"PyRetro\"."
dpkg-deb --build temp_package
mv temp_package.deb pyRetro_$pyretro_version.deb


echo "Delete the \"temp_package\" temporary dir for job."
rm -Rf temp_package

echo "DONE: Package ready at: ../pyRetro_$pyretro_version.deb"
